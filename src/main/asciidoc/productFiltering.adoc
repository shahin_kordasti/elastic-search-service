== Product Filtering

=== Get all filters for type

Executing a filter request with no filters, for a particular item type, to get all items and filters for that type.

.Request
include::{snippets}/should-get-filters-for-lenses/http-request.adoc[]
.Response - HTTP 200 with search result (arrays may be truncated)
include::{snippets}/should-get-filters-for-lenses/http-response.adoc[]

=== Filter on Lenses

Filtering Lenses by providing key/values from aggregations[].field/aggregations[].buckets[].value.
Also note that filtering does not change the aggregations, as they are applied after.

Observe that since this filter request is done on two different property groups then the result is combined. E.g. filtering brand "The Collection"
and sup type "Endagslinser" will only return "Endagslinser" which is of that brand.

.Request
include::{snippets}/should-filter-on-lenses/http-request.adoc[]
.Response - HTTP 200 with search result (arrays may be truncated)
include::{snippets}/should-filter-on-lenses/http-response.adoc[]

=== Filter on Lenses within same property group

When filtering with multiple values in the same group then the result is additive. E.g. filtering on brand "The Collection" and "Polo Ralph Lauren"
will include both in the result.

.Request
include::{snippets}/should-filter-on-frames-in-same-property-group/http-request.adoc[]
.Response - HTTP 200 with search result (arrays may be truncated)
include::{snippets}/should-filter-on-frames-in-same-property-group/http-response.adoc[]
