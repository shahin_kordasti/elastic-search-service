$(document).ready(function() {
    $(function() {
        $("#searchBox").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/product-service/product/suggestions",
                    type: "GET",
                    data: { prefix: request.term },

                    dataType: "json",

                    success: function(data) {
                        response($.map(data, function(v){
                            return {
                                value: v
                            };
                        }));
                    }
                });
            }
        });
    });
});