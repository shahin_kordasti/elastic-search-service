package org.springframework.data.elasticsearch.core;

import com.lenswaygroup.service.product.domain.ProductSearch;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.springframework.data.elasticsearch.ElasticsearchException;
import org.springframework.data.elasticsearch.core.mapping.ElasticsearchPersistentEntity;
import org.springframework.data.elasticsearch.core.mapping.ElasticsearchPersistentProperty;
import org.springframework.stereotype.Component;

import static org.springframework.data.elasticsearch.core.MappingBuilder.buildMapping;

/**
 * This class exists so we can get access to package private method {@link MappingBuilder#buildMapping(Class, String,
 * String, String)} to build mappings from entity objects so that we can execute {@link
 * ElasticsearchOperations#putMapping(String, String, Object)}
 */
@Component
public class MappingBuilderProxy {

    public XContentBuilder getMappings(ElasticsearchPersistentEntity persistentEntity,
                                       Class<ProductSearch> productSearchClass) {
        XContentBuilder xContentBuilder = null;
        try {

            ElasticsearchPersistentProperty property =
                    (ElasticsearchPersistentProperty) persistentEntity.getRequiredIdProperty();

            xContentBuilder = buildMapping(productSearchClass, persistentEntity.getIndexType(), property.getFieldName(),
                    persistentEntity.getParentType());
        } catch (Exception e) {
            throw new ElasticsearchException("Failed to build mapping for " + productSearchClass.getSimpleName(), e);
        }
        return xContentBuilder;
    }
}
