package com.lenswaygroup.service.product.converter.aggregation;

import com.lenswaygroup.service.product.domain.AggregationBucket;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class StringTermsWrapper extends AggregationWrapper {
    private StringTerms aggregation;

    public StringTermsWrapper(StringTerms aggregation) {
        this.aggregation = aggregation;
    }

    @Override
    public List<AggregationBucket> getBuckets() {
        return aggregation.getBuckets().stream()
                .map(bucket -> AggregationBucket.builder().value(bucket.getKeyAsString()).count(bucket.getDocCount())
                        .build()).collect(toList());
    }
}
