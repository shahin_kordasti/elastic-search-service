package com.lenswaygroup.service.product.converter;

import com.lenswaygroup.service.product.controller.dto.ProductSearchResultDTO;
import com.lenswaygroup.service.product.controller.dto.SearchRequestDTO;
import com.lenswaygroup.service.product.domain.ProductSearchResult;
import com.lenswaygroup.service.product.domain.SearchTextRequest;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ProductSearchConverter {

    private final ModelMapper modelMapper;

    public SearchTextRequest toEntity(SearchRequestDTO searchRequestDTO) {
        return modelMapper.map(searchRequestDTO, SearchTextRequest.class);
    }

    public ProductSearchResultDTO toDTO(ProductSearchResult searchResult) {
        return modelMapper.map(searchResult, ProductSearchResultDTO.class);
    }
}
