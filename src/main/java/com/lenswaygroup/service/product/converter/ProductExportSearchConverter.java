package com.lenswaygroup.service.product.converter;

import com.lenswaygroup.service.product.domain.ProductSearch;
import com.lenswaygroup.service.product.integration.dto.ProductExportDTO;
import com.lenswaygroup.service.product.integration.dto.ProductPropertyValueDTO;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@RequiredArgsConstructor
@Component
public class ProductExportSearchConverter {
    private static final String COLORS_PROP_NAME = "colors";
    public static final String TAGS_PROP_NAME = "searchTags";
    private static final String BRAND_FAMILY_NAME_PROP_NAME = "brandFamilyName";
    public static final List<Integer> USAGE_TYPE_IDS = asList(73, 74, 78, 80, 85, 87, 92, 97, 99);

    private final ModelMapper modelMapper;

    public ProductSearch toEntity(ProductExportDTO productExportDTO) {
        ProductSearch entitySearch = modelMapper.map(productExportDTO, ProductSearch.class);
        mapProperties(productExportDTO, entitySearch);
        mapSubType(productExportDTO, entitySearch);
        mapColors(productExportDTO, entitySearch);
        mapBrandFamilyName(productExportDTO, entitySearch);
        mapTags(productExportDTO, entitySearch);
        return entitySearch;
    }

    private void mapTags(ProductExportDTO productExportDTO, ProductSearch entitySearch) {
        String tagStr = getPropertyValue(productExportDTO.getPropertyValues(), TAGS_PROP_NAME);
        if (isNotEmpty(tagStr)) {
            List<String> tags = stream(tagStr.split(",")).map(tag -> tag.trim()).collect(toList());
            entitySearch.setTags(tags);
        }
    }

    private void mapSubType(ProductExportDTO productExportDTO, ProductSearch entitySearch) {
        if (productExportDTO.getSubTypes() != null) {
            List<String> subTypes = new ArrayList<>();
            List<String> usageTypes = new ArrayList<>();
            for (int i = 0; i < productExportDTO.getSubTypes().getIds().size(); i++) {
                int id = productExportDTO.getSubTypes().getIds().get(i);
                String i18Value = safeGetValue(productExportDTO.getSubTypes().getI18nValues(), i);
                if (USAGE_TYPE_IDS.contains(id)) {
                    usageTypes.add(i18Value);
                } else {
                    subTypes.add(i18Value);
                }
            }
            entitySearch.setSubTypes(subTypes);
            entitySearch.setUsageTypes(usageTypes);
        }
    }

    private String safeGetValue(List<String> values, int i) {
        return CollectionUtils.isNotEmpty(values) && values.size() > i ? values.get(i) : null;
    }

    private void mapColors(ProductExportDTO productExportDTO, ProductSearch entitySearch) {
        entitySearch.setColors(getPropertyValue(productExportDTO.getPropertyValues(), COLORS_PROP_NAME));
    }

    private void mapBrandFamilyName(ProductExportDTO productExportDTO, ProductSearch entitySearch) {
        entitySearch.setBrandFamilyName(
                getPropertyValue(productExportDTO.getPropertyValues(), BRAND_FAMILY_NAME_PROP_NAME));
    }

    private String getPropertyValue(List<ProductPropertyValueDTO> propertyValues, String propName) {
        return propertyValues.stream().filter(prop -> propName.equals(prop.getName())).map(prop -> prop.getValue())
                .findAny().orElse(null);
    }

    private void mapProperties(ProductExportDTO productExportDTO, ProductSearch entitySearch) {
        entitySearch.setPropertyMap(productExportDTO.getPropertyValues().stream()
                .collect(Collectors.toMap(ProductPropertyValueDTO::getName, ProductPropertyValueDTO::getValue)));
    }
}
