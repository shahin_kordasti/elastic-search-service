package com.lenswaygroup.service.product.converter.aggregation;

import com.lenswaygroup.service.product.domain.AggregationBucket;
import com.lenswaygroup.service.product.domain.AggregationContainer;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.lenswaygroup.service.product.domain.IndexedFields.nameFromField;
import static java.util.Collections.emptyList;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

@Log4j2
@RequiredArgsConstructor
@Component
public class AggregationConverter {

    public List<AggregationContainer> toAggregations(Aggregations aggregationsIn) {
        List<AggregationContainer> aggregationsOut = new ArrayList<>();
        for (String aggregationField : aggregationsIn.getAsMap().keySet()) {
            Aggregation aggregation = aggregationsIn.getAsMap().get(aggregationField);
            List<AggregationBucket> aggregationBuckets = toAggregationBuckets(aggregation);
            if (isEmpty(aggregationBuckets)) {
                LOGGER.info("Could not extract buckets from aggregation: " + aggregation);
                continue;
            }
            //TODO internationalize the aggregation name
            aggregationsOut
                    .add(AggregationContainer.builder().name(nameFromField(aggregationField)).field(aggregationField)
                                             .buckets(aggregationBuckets).build());
        }
        return aggregationsOut;
    }

    public List<AggregationBucket> toAggregationBuckets(Aggregation aggregation) {
        Optional<AggregationWrapper> aggrWrapper = AggregationWrapper.create(aggregation);
        return aggrWrapper.map(wrapper -> wrapper.getBuckets()).orElse(emptyList());
    }
}
