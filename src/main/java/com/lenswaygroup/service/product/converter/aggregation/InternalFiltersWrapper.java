package com.lenswaygroup.service.product.converter.aggregation;

import com.lenswaygroup.service.product.domain.AggregationBucket;
import org.elasticsearch.search.aggregations.bucket.filter.InternalFilters;

import java.util.List;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

public class InternalFiltersWrapper extends AggregationWrapper {
    private static final Pattern NUMBER_PATTERN = Pattern.compile("\\d+");
    private InternalFilters aggregation;

    public InternalFiltersWrapper(InternalFilters aggregation) {
        this.aggregation = aggregation;
    }

    @Override
    public List<AggregationBucket> getBuckets() {
        List<AggregationBucket> buckets = aggregation.getBuckets().stream().filter(bucket -> bucket.getDocCount() > 0)
                .map(bucket -> AggregationBucket.builder().value(bucket.getKeyAsString()).count(bucket.getDocCount())
                        .build()).collect(toList());
        // this is because ElasticSearch filter buckets are returned in the same order as provided in the request
        // but the ElasticSearch Java library sorts it by natural order of the key (String), which is descending.
        buckets.sort((AggregationBucket bck1, AggregationBucket bck2) -> bck2.getValue().compareTo(bck1.getValue()));
        return buckets;
    }
}
