package com.lenswaygroup.service.product.converter.aggregation;

import com.lenswaygroup.service.product.domain.AggregationBucket;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.InternalFilters;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;

import java.util.List;
import java.util.Optional;

public abstract class AggregationWrapper {

    public static Optional<AggregationWrapper> create(Aggregation aggregation) {
        if (aggregation instanceof StringTerms) {
            return Optional.of(new StringTermsWrapper((StringTerms) aggregation));
        } else if (aggregation instanceof InternalFilters) {
            return Optional.of(new InternalFiltersWrapper((InternalFilters) aggregation));
        }
        return Optional.empty();
    }

    public abstract List<AggregationBucket> getBuckets();
}
