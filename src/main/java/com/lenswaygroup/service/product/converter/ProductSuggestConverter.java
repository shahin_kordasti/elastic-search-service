package com.lenswaygroup.service.product.converter;

import com.lenswaygroup.service.product.controller.dto.SuggestRequestDTO;
import com.lenswaygroup.service.product.domain.SuggestRequest;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ProductSuggestConverter {

    private final ModelMapper modelMapper;

    public SuggestRequest toEntity(SuggestRequestDTO suggestRequestDTO) {
        return modelMapper.map(suggestRequestDTO, SuggestRequest.class);
    }
}
