package com.lenswaygroup.service.product.converter;

import com.lenswaygroup.service.product.controller.dto.FilterRequestDTO;
import com.lenswaygroup.service.product.domain.FilterRequest;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ProductFilterConverter {

    private final ModelMapper modelMapper;

    public FilterRequest toEntity(FilterRequestDTO filterRequestDTO) {
        return modelMapper.map(filterRequestDTO, FilterRequest.class);
    }
}
