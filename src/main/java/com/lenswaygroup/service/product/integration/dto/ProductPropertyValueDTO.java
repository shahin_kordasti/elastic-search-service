package com.lenswaygroup.service.product.integration.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductPropertyValueDTO {
    @NotNull
    private Long id;
    private String name;
    @NotNull
    private String value;
}
