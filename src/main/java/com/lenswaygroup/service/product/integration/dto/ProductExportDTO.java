package com.lenswaygroup.service.product.integration.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductExportDTO {
    private String id;
    private Long productgroupId;
    private String productName;
    private String brand;
    private String type;
    private String i18nType;
    private SubTypesDTO subTypes;
    private Double price;
    private List<ProductPropertyValueDTO> propertyValues = new ArrayList<>();

    private String largeImageUrl;
    private String extraLargeImageUrl;
    private String originalImageUrl;
    private String originalImageUrlRelative;
    private String thumbnailUrl;
    private String listImageUrl;
}
