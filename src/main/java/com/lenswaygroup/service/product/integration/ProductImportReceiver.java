package com.lenswaygroup.service.product.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lenswaygroup.service.product.integration.dto.ProductExportEventDTO;
import com.lenswaygroup.service.product.service.ProductIndexingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static com.lenswaygroup.integration.rabbitmq.config.ProductsMessaging.PRODUCTS_EXPORT_QUEUE;
import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@Log4j2
public class ProductImportReceiver {

    private final ProductIndexingService indexingService;
    private final ObjectMapper objectMapper;

    @RabbitListener(queues = PRODUCTS_EXPORT_QUEUE)
    public void receiveEvent(ProductExportEventDTO productExportEventDTO) throws Exception {
        LOGGER.info("Received ProductExportEventDTO {}", productExportEventDTO);
//        writeEventToFile(productExportEventDTO);
        indexingService.indexAllProducts(productExportEventDTO);
    }

    private void writeEventToFile(ProductExportEventDTO productExportEventDTO) throws IOException {
        FileUtils.writeStringToFile(new File(format("json/products-%d%s.json", productExportEventDTO.getOrgId(),
                productExportEventDTO.getCountryCode())), objectMapper.writeValueAsString(productExportEventDTO),
                Charset.defaultCharset());
    }
}
