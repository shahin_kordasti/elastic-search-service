package com.lenswaygroup.service.product.integration.dto;

import com.lenswaygroup.integration.rabbitmq.event.EventDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProductExportEventDTO extends EventDTO {
    private List<ProductExportDTO> productExports;
    private Integer orgId;
    private String countryCode;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("orgId", orgId).append("countryCode", countryCode)
                .append("nr of exported products", nrOfProductExports()).toString();
    }

    private int nrOfProductExports() {
        return productExports != null ? productExports.size() : 0;
    }
}
