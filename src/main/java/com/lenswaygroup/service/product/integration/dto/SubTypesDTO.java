package com.lenswaygroup.service.product.integration.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubTypesDTO {
    private List<Integer> ids;
    private List<String> values;
    private List<String> i18nValues;
}
