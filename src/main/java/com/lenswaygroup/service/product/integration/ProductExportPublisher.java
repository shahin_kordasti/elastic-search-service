package com.lenswaygroup.service.product.integration;

import com.lenswaygroup.service.product.integration.dto.ProductExportEventDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import static com.lenswaygroup.integration.rabbitmq.config.ProductsMessaging.PRODUCTS_ROUTING_WILDCARD;


@RequiredArgsConstructor
@Service
@Profile(value = {"stub", "component-test"})
public class ProductExportPublisher {
    private final RabbitTemplate rabbitTemplate;

    public void publishEvent(ProductExportEventDTO productImportEventDTO) {
        rabbitTemplate.convertAndSend(PRODUCTS_ROUTING_WILDCARD, productImportEventDTO);
    }
}
