package com.lenswaygroup.service.product.controller;

import com.lenswaygroup.service.product.controller.dto.AggregationsDTO;
import com.lenswaygroup.service.product.controller.dto.FilterRequestDTO;
import com.lenswaygroup.service.product.controller.dto.ProductSearchResultDTO;
import com.lenswaygroup.service.product.controller.dto.SearchRequestDTO;
import com.lenswaygroup.service.product.converter.ProductFilterConverter;
import com.lenswaygroup.service.product.converter.ProductSearchConverter;
import com.lenswaygroup.service.product.domain.ItemType;
import com.lenswaygroup.service.product.domain.SearchIdRequest;
import com.lenswaygroup.service.product.service.ProductSearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.lenswaygroup.service.product.util.ValueUtil.getParam;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.EMPTY;

@RequiredArgsConstructor
@Log4j2
@Controller
public class ProductSearchWebController {

    public static final String SEARCH_TEXT = "searchText";
    public static final String ITEM_TYPE = "itemType";
    private final ProductSearchService productSearchService;
    private final ProductSearchConverter productSearchConverter;
    private final ProductFilterConverter filterConverter;

    @GetMapping("/product/search")
    public String productSearch(@RequestParam(value = SEARCH_TEXT, required = false) String searchText,
                                @RequestParam MultiValueMap<String, String> filters, Model model) {
        ProductSearchResultDTO searchResult = ProductSearchResultDTO.builder().build();
        //all params beside searchText is assumed to be filters
        filters.remove(SEARCH_TEXT);
        if (isNotBlank(searchText)) {
            SearchRequestDTO searchQueryDTO =
                    SearchRequestDTO.builder().searchText(searchText).filters(filters).countryCode("SE").orgId(83)
                            .build();
            searchResult = productSearchConverter
                    .toDTO(productSearchService.search(productSearchConverter.toEntity(searchQueryDTO)));

        }
        model.addAttribute("searchResult", searchResult);
        model.addAttribute(SEARCH_TEXT, getParam(searchText, EMPTY));
        model.addAttribute("filters", filters);
        return "productlist";
    }

    @GetMapping("/product/filter")
    public String productFilter(@RequestParam(value = ITEM_TYPE) ItemType itemType,
                                @RequestParam MultiValueMap<String, String> filters, Model model) {
        //all params beside itemType is assumed to be filters
        filters.remove(ITEM_TYPE);
        FilterRequestDTO searchQueryDTO =
                FilterRequestDTO.builder().type(itemType).filters(filters).countryCode("SE").orgId(83).build();
        ProductSearchResultDTO filterResult =
                productSearchConverter.toDTO(productSearchService.filter(filterConverter.toEntity(searchQueryDTO)));

        model.addAttribute("searchResult", filterResult);
        model.addAttribute(ITEM_TYPE, itemType);
        model.addAttribute("filters", filters);
        return "productlist";
    }

    @GetMapping("/product/view")
    public String productInfo(@RequestParam(value = "id") String id,
                              @RequestParam(value = SEARCH_TEXT, required = false) String searchText, Model model) {
        model.addAttribute(SEARCH_TEXT, getParam(searchText, EMPTY));
        ProductSearchResultDTO searchResult = productSearchConverter.toDTO(productSearchService
                .findBy(SearchIdRequest.builder().id(id).countryCode("SE").orgId(83).build()));
        model.addAttribute("searchResult", searchResult);
        model.addAttribute("product", searchResult.getHits().get(0));
        return "productview.html";
    }

    //helper methods called from Thymeleaf templates
    public String filterUrl(String filterKey, String filterValue) {
        Map<String, String[]> paramMap =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getParameterMap();
        String[] filterValues = {filterValue};
        if (isFilterAndValueSet(filterKey, filterValue, paramMap)) {
            filterValues = asList(paramMap.get(filterKey)).stream().filter(value -> !value.equals(filterValue))
                    .toArray(size -> new String[size]);
        } else if (isNotEmpty(paramMap.get(filterKey))) {
            ArrayList<String> existingVals = new ArrayList<>(asList(paramMap.get(filterKey)));
            existingVals.add(filterValue);
            filterValues = existingVals.toArray(new String[0]);
        }
        return ServletUriComponentsBuilder.fromCurrentRequest().replaceQueryParam(filterKey, filterValues).build()
                .toString();
    }

    public Boolean isFilterSelected(String filterKey, String filterValue) {
        Map<String, String[]> paramMap =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getParameterMap();
        if (isFilterAndValueSet(filterKey, filterValue, paramMap)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isFilterAndValueSet(String filterKey, String filterValue, Map<String, String[]> paramMap) {
        return isNotEmpty(paramMap.get(filterKey)) &&
                asList(paramMap.get(filterKey)).stream().anyMatch(value -> value.equals(filterValue));
    }

    public String clearUrl() {
        Map<String, String[]> paramMap =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getParameterMap();
        MultiValueMap<String, String> clearedMap = new LinkedMultiValueMap<>();
        if (paramMap.get(ITEM_TYPE) != null) {
            clearedMap.put(ITEM_TYPE, asList(paramMap.get(ITEM_TYPE)));
        }
        if (paramMap.get(SEARCH_TEXT) != null) {
            clearedMap.put(SEARCH_TEXT, asList(paramMap.get(SEARCH_TEXT)));
        }
        ServletUriComponentsBuilder uriBuilder = ServletUriComponentsBuilder.fromCurrentRequest();
        uriBuilder.replaceQueryParams(clearedMap);
        return uriBuilder.build().toString();
    }

    public String getFilterName(String field, List<AggregationsDTO> aggregations) {
        return aggregations.stream().filter(aggr -> aggr.getField().equals(field)).map(aggr -> aggr.getName()).findAny()
                .orElse(field);
    }
}
