package com.lenswaygroup.service.product.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "baseRequestBuilder")
public class BaseRequestDTO {
    @Min(1)
    private Integer orgId;
    @NotBlank
    private String countryCode;

    private Map<String, List<String>> filters;
}
