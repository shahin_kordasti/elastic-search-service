package com.lenswaygroup.service.product.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SearchRequestDTO extends BaseRequestDTO {
    @NotBlank
    private String searchText;

    @Builder
    public SearchRequestDTO(@Min(1) Integer orgId, @NotBlank String countryCode, Map<String, List<String>> filters,
                            @NotBlank String searchText) {
        super(orgId, countryCode, filters);
        this.searchText = searchText;
    }
}
