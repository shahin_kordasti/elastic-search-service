package com.lenswaygroup.service.product.controller.dto;

import com.lenswaygroup.service.product.domain.ItemType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FilterRequestDTO extends BaseRequestDTO {
    @NotNull
    private ItemType type;

    @Builder
    public FilterRequestDTO(@Min(1) Integer orgId, @NotBlank String countryCode, Map<String, List<String>> filters,
                            @NotNull ItemType type) {
        super(orgId, countryCode, filters);
        this.type = type;
    }
}
