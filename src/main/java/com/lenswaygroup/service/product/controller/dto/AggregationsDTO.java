package com.lenswaygroup.service.product.controller.dto;

import com.lenswaygroup.service.product.domain.AggregationBucket;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AggregationsDTO {
    private String name;
    private String field;
    private List<AggregationBucket> buckets;
}
