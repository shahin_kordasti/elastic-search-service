package com.lenswaygroup.service.product.controller;

import com.lenswaygroup.service.product.controller.dto.FilterRequestDTO;
import com.lenswaygroup.service.product.controller.dto.ProductSearchResultDTO;
import com.lenswaygroup.service.product.converter.ProductFilterConverter;
import com.lenswaygroup.service.product.converter.ProductSearchConverter;
import com.lenswaygroup.service.product.service.ProductSearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@Log4j2
@RestController
@RequestMapping("/product")
public class ProductFilterController {

    private final ProductSearchService productSearchService;
    private final ProductFilterConverter filterConverter;
    private final ProductSearchConverter productSearchConverter;

    @PostMapping("/filters")
    public ResponseEntity<ProductSearchResultDTO> filterProducts(@RequestBody FilterRequestDTO filtersRequest) {
        ProductSearchResultDTO resultDTO =
                productSearchConverter.toDTO(productSearchService.filter(filterConverter.toEntity(filtersRequest)));
        return ResponseEntity.ok(resultDTO);
    }
}
