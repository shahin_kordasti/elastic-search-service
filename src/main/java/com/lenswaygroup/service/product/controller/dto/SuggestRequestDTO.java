package com.lenswaygroup.service.product.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.HashMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SuggestRequestDTO extends BaseRequestDTO {
    @NotEmpty
    private String prefix;

    @Builder
    public SuggestRequestDTO(@Min(1) Integer orgId, @NotBlank String countryCode, @NotEmpty String prefix) {
        super(orgId, countryCode, new HashMap<>());
        this.prefix = prefix;
    }
}
