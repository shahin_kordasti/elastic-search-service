package com.lenswaygroup.service.product.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductSearchDTO {
    private String id;
    private Long productgroupId;
    private String productName;
    private String brand;
    private String brandFamilyName;
    private String type;
    private String i18nType;
    private List<String> usageTypes;
    private List<String> subTypes;
    private List<String> tags;
    private String colors;
    private Double price;

    private Map<String, String> propertyMap;

    private String largeImageUrl;
    private String extraLargeImageUrl;
    private String originalImageUrl;
    private String originalImageUrlRelative;
    private String thumbnailUrl;
    private String listImageUrl;

    public String getListImageUrl() {
        if (listImageUrl == null || listImageUrl.startsWith("data:")) {
            return originalImageUrl;
        } else {
            return listImageUrl;
        }
    }
}
