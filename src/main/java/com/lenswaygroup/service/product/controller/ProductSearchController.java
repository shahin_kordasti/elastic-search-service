package com.lenswaygroup.service.product.controller;

import com.lenswaygroup.service.product.controller.dto.ProductSearchResultDTO;
import com.lenswaygroup.service.product.controller.dto.SearchRequestDTO;
import com.lenswaygroup.service.product.converter.ProductSearchConverter;
import com.lenswaygroup.service.product.service.ProductSearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@Log4j2
@RestController
@RequestMapping("/product")
public class ProductSearchController {

    private final ProductSearchService productSearchService;
    private final ProductSearchConverter productSearchConverter;

    @PostMapping("/search")
    public ResponseEntity<ProductSearchResultDTO> searchProducts(@RequestBody SearchRequestDTO searchReqDTO) {
        ProductSearchResultDTO searchResult = productSearchConverter
                .toDTO(productSearchService.search(productSearchConverter.toEntity(searchReqDTO)));
        return ResponseEntity.ok(searchResult);
    }
}
