package com.lenswaygroup.service.product.controller;

import com.lenswaygroup.service.product.controller.dto.SuggestRequestDTO;
import com.lenswaygroup.service.product.converter.ProductSuggestConverter;
import com.lenswaygroup.service.product.domain.SuggestRequest;
import com.lenswaygroup.service.product.service.ProductAutosuggestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@Log4j2
@RestController
@RequestMapping("/product")
public class ProductAutosuggestController {

    private final ProductAutosuggestService productAutosuggestService;
    private final ProductSuggestConverter productSuggestConverter;

    @GetMapping("/suggestions")
    public ResponseEntity<List<String>> productAutocomplete(@RequestParam(value = "prefix") String prefix) {
        return ResponseEntity.ok(productAutosuggestService.getSuggestions(new SuggestRequest(83, "SE", prefix)));
    }

    @PostMapping("/suggestions")
    public ResponseEntity<List<String>> productAutocomplete(@RequestBody SuggestRequestDTO suggestRequest) {
        return ResponseEntity
                .ok(productAutosuggestService.getSuggestions(productSuggestConverter.toEntity(suggestRequest)));
    }
}
