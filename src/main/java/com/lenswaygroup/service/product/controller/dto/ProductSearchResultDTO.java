package com.lenswaygroup.service.product.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductSearchResultDTO {
    private List<ProductSearchDTO> hits;
    private Long took;
    private List<AggregationsDTO> aggregations;

    public Long getTook() {
        return took != null ? took : -1;
    }

    public List<ProductSearchDTO> getHits() {
        return hits != null ? hits : new ArrayList<>();
    }
}
