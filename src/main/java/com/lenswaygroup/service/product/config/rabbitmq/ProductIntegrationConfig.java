package com.lenswaygroup.service.product.config.rabbitmq;

import com.lenswaygroup.integration.rabbitmq.config.RabbitIntegrationConfig;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.lenswaygroup.integration.rabbitmq.config.ProductsMessaging.PRODUCTS_EXPORT_QUEUE;
import static com.lenswaygroup.integration.rabbitmq.config.ProductsMessaging.PRODUCTS_ROUTING_WILDCARD;

@Configuration
public class ProductIntegrationConfig extends RabbitIntegrationConfig {

    @Bean
    Queue productsQueue() {
        return createQueueWithDLQ(PRODUCTS_EXPORT_QUEUE);
    }

    @Bean
    Binding productsBinding(TopicExchange topicExchange) {
        return BindingBuilder.bind(productsQueue()).to(topicExchange).with(PRODUCTS_ROUTING_WILDCARD);
    }

}
