package com.lenswaygroup.service.product.config.elastic;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.MappingBuilderProxy;

@Configuration
public class ElasticSearchConfig {

    @Bean
    public MappingBuilderProxy mappingBuilderProxy(){
        return new MappingBuilderProxy();
    }

}
