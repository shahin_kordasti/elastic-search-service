package com.lenswaygroup.service.product.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lenswaygroup.service.product.integration.ProductExportPublisher;
import com.lenswaygroup.service.product.integration.dto.ProductExportEventDTO;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.ChannelCallback;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import static com.lenswaygroup.integration.rabbitmq.config.RabbitIntegrationConfig.OPTI_PORTAL_TOPIC_EXCHANGE;
import static java.util.Arrays.asList;

/**
 * This class is used to index Elastic Search with stubbed data loaded from a file ({@link
 * ElasticStubIndexer#PRODUCTS_INDEX_FILE}). Used primarily for testing (spring profile 'component-test') or when you
 * want to have some initial elastic search index data (spring profile 'stub'). Should not be used in other cases (like
 * prod).<p> Observe that it checks that the relevant Rabbit MQ Exchange exists and that Elastic Search is up and
 * running before it publishes the product index Event.
 */
@Profile(value = {"stub", "component-test"})
@Component
@Log4j2
@RequiredArgsConstructor
public class ElasticStubIndexer {
    private static final int CHECK_MAX_WAIT_MS = 5000;
    private static final int CHECK_BACKOFF_MS = 500;
    private static final String PRODUCTS_INDEX_FILE = "products-83SE.json";

    private final ObjectMapper objectMapper;
    private final ProductExportPublisher prodExportPublisher;
    private final Environment env;
    private final RabbitTemplate rabbitTemplate;
    private final HealthIndicator elasticsearchHealthIndicator;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) throws Exception {
        if (asList(env.getActiveProfiles()).contains("stub")) {
            indexStubData();
        }
    }

    public void indexStubData() throws Exception {
        checkExchangeExists();
        checkElasticRunning();
        LOGGER.info("Triggering indexing Elastic with stubbed data");
        ProductExportEventDTO importedProductEvent = objectMapper
                .readValue(getClass().getClassLoader().getResourceAsStream(PRODUCTS_INDEX_FILE),
                        ProductExportEventDTO.class);
        prodExportPublisher.publishEvent(importedProductEvent);
    }

    private void checkElasticRunning() {
        long time = System.currentTimeMillis();
        while (!elasticRunning() && (System.currentTimeMillis() - time < CHECK_MAX_WAIT_MS)) {
        }
        if (!elasticRunning()) {
            throw new RuntimeException("Elastic Search Server was not found running...");
        }
    }

    private boolean elasticRunning() {
        sleep(CHECK_BACKOFF_MS);
        return "UP".equals(elasticsearchHealthIndicator.health().getStatus().getCode());
    }

    private void checkExchangeExists() {
        long time = System.currentTimeMillis();
        while (!exchangeExists() && (System.currentTimeMillis() - time < CHECK_MAX_WAIT_MS)) {
        }
        if (!exchangeExists()) {
            throw new RuntimeException(OPTI_PORTAL_TOPIC_EXCHANGE + " exchange was not created...");
        }
    }

    private boolean exchangeExists() {
        return rabbitTemplate.execute(new ChannelCallback<AMQP.Exchange.DeclareOk>() {
            @Override
            public AMQP.Exchange.DeclareOk doInRabbit(Channel channel) throws Exception {
                try {
                    AMQP.Exchange.DeclareOk declareOk = channel.exchangeDeclarePassive(OPTI_PORTAL_TOPIC_EXCHANGE);
                    LOGGER.debug("Exchange [{}] exists: [{}]", OPTI_PORTAL_TOPIC_EXCHANGE, declareOk != null);
                    return declareOk;
                } catch (Exception e) {
                    LOGGER.debug("Exchange [{}] does not exist", OPTI_PORTAL_TOPIC_EXCHANGE);
                    sleep(CHECK_BACKOFF_MS);
                    return null;
                }
            }
        }) != null;
    }

    public void sleep(int sleepInMs) {
        try {
            Thread.sleep(sleepInMs);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
