package com.lenswaygroup.service.product.util;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.Charset;

public class ResourceUtil {

    public static String classPathResourceToString(String resourceLoc) {
        try {
            return IOUtils.toString(Thread.currentThread().getContextClassLoader().getResource(resourceLoc), Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException("Could not read classpath resource at: " + resourceLoc, e);
        }
    }
}
