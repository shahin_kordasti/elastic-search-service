package com.lenswaygroup.service.product.util;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class ValueUtil {

    public static String getParam(String param, String defaultValue) {
        return param != null ? param : defaultValue;
    }

    /**
     * Recursively tokenizes a string value with each token being one 'word' smaller than the previous by removing the
     * first word. E.g. <pre>"1-day Acuvue Moist"</pre> will be tokenized into
     * <pre>["1-day Acuvue Moist", "Acuvue Moist", "Moist"]</pre>
     */
    public static String[] tokenize(String fieldValue) {
        if (StringUtils.isBlank(fieldValue)) {
            return new String[0];
        }
        String[] tokenizedField = fieldValue.split(" ");
        if (tokenizedField.length == 1) {
            return tokenizedField;
        }
        return ArrayUtils
                .addAll(new String[]{fieldValue}, tokenize(fieldValue.substring(tokenizedField[0].length()).trim()));
    }
}
