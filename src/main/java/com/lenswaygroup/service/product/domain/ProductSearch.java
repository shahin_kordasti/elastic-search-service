package com.lenswaygroup.service.product.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.CompletionField;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.InnerField;
import org.springframework.data.elasticsearch.annotations.MultiField;
import org.springframework.data.elasticsearch.core.completion.Completion;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "product", createIndex = false)
public class ProductSearch {
    @Id
    private String id;
    private Long productgroupId;
    private String productName;
    @MultiField(mainField = @Field(type = FieldType.Keyword, store = true),
            otherFields = {@InnerField(suffix = "raw", type = FieldType.Text, analyzer = "trigrams")})
    private String brand;
    @Field(type = FieldType.Text, analyzer = "trigrams")
    private String brandFamilyName;
    @Field(type = FieldType.Keyword)
    private String type;
    @Field(type = FieldType.Keyword)
    private String i18nType;
    @MultiField(mainField = @Field(type = FieldType.Keyword, store = true),
            otherFields = {@InnerField(suffix = "raw", type = FieldType.Text, analyzer = "trigrams")})
    private List<String> usageTypes;
    @MultiField(mainField = @Field(type = FieldType.Keyword, store = true),
            otherFields = {@InnerField(suffix = "raw", type = FieldType.Text, analyzer = "trigrams")})
    private List<String> subTypes;
    @MultiField(mainField = @Field(type = FieldType.Keyword, store = true),
            otherFields = {@InnerField(suffix = "raw", type = FieldType.Text, analyzer = "trigrams")})
    private List<String> tags;

    @Field(type = FieldType.Keyword)
    private String colors;
    private Double price;
    private Map<String, String> propertyMap;

    private String largeImageUrl;
    private String extraLargeImageUrl;
    private String originalImageUrl;
    private String originalImageUrlRelative;
    private String thumbnailUrl;
    private String listImageUrl;

    @CompletionField
    private Completion suggestions;
}
