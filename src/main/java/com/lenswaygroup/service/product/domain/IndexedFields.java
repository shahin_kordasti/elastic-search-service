package com.lenswaygroup.service.product.domain;

import com.lenswaygroup.service.product.domain.fields.IndexedField;
import com.lenswaygroup.service.product.domain.fields.PriceField;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.lenswaygroup.service.product.domain.ItemType.EXTRA;
import static com.lenswaygroup.service.product.domain.ItemType.FRAME;
import static com.lenswaygroup.service.product.domain.ItemType.LENS;
import static com.lenswaygroup.service.product.domain.ItemType.SUNWEAR;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.elasticsearch.common.unit.Fuzziness.AUTO;
import static org.elasticsearch.common.unit.Fuzziness.ZERO;

public class IndexedFields {
    public static final String KEYWORD_SUFFIX = ".keyword";
    public static final String PROPERTY_MAP_PREFIX = "propertyMap.";

    public static final IndexedField PRICE = new PriceField("price", "Price", 12f, AUTO);
    public static final IndexedField ITEM_TYPE = new IndexedField("type", "Type", 12f, ZERO);
    public static final IndexedField I18N_TYPE = new IndexedField("i18nType", "Item Type", 20f, AUTO, 10f, ProductSearch::getI18nType);
    public static final IndexedField SUB_TYPES = new IndexedField("subTypes", "Sub Type", 12f, AUTO);
    public static final IndexedField SUB_TYPES_RAW =
            new IndexedField("subTypes.raw", "Sub Type (raw)", 0.1f, AUTO, 0.1f);
    public static final IndexedField USAGE_TYPES = new IndexedField("usageTypes", "Usage Type", 12f, AUTO);
    public static final IndexedField USAGE_TYPES_RAW =
            new IndexedField("usageTypes.raw", "Usage Type (raw)", 0.1f, AUTO, 0.1f);
    public static final IndexedField BRAND = new IndexedField("brand", "Brand", 10f, AUTO, ProductSearch::getBrand);
    public static final IndexedField BRAND_RAW = new IndexedField("brand.raw", "Brand (raw)", 5f, AUTO, 0.5f);
    public static final IndexedField I18N_COLOR = new IndexedField("propertyMap.colors", "i18n Color", 11f, AUTO);
    public static final IndexedField COLOR = new IndexedField("propertyMap.color", "Color", 9f, AUTO);
    public static final IndexedField SHAPE = new IndexedField("propertyMap.shape", "Shape", 10f, AUTO);
    public static final IndexedField GENDER = new IndexedField("propertyMap.gender", "Gender", 10f, ZERO);
    public static final IndexedField PRODUCT_NAME = new IndexedField("productName", "Product Name", 10f, AUTO, ProductSearch::getProductName);
    public static final IndexedField BRAND_FAMILY =
            new IndexedField("propertyMap.brandFamilyName", "Brand Family", 10f, AUTO);
    public static final IndexedField BRAND_FAMILY_RAW =
            new IndexedField("brandFamilyName", "Brand Family (raw)", 3f, AUTO, 2f, ProductSearch::getBrandFamilyName);
    public static final IndexedField MATERIAL = new IndexedField("propertyMap.material", "Material", 10f, ZERO);
    public static final IndexedField TAGS = new IndexedField("tags", "Tags", 14f, AUTO);
    public static final IndexedField TAGS_RAW = new IndexedField("tags.raw", "Tags (raw)", 1f, AUTO, 1f);

    public static final List<IndexedField> ALL =
            asList(PRICE, ITEM_TYPE, PRODUCT_NAME, BRAND, BRAND_RAW, BRAND_FAMILY, BRAND_FAMILY_RAW, I18N_TYPE, TAGS,
                    TAGS_RAW, SUB_TYPES, SUB_TYPES_RAW, USAGE_TYPES, USAGE_TYPES_RAW, I18N_COLOR, COLOR, SHAPE, GENDER,
                    MATERIAL);

    public static final List<IndexedField> SEARCH_FIELDS = new ArrayList<>(ALL);
    static {
        SEARCH_FIELDS.remove(PRICE);
        SEARCH_FIELDS.remove(ITEM_TYPE);
    }

    public static final List<IndexedField> SEARCH_AGGREGATIONS =
            asList(I18N_TYPE, USAGE_TYPES, SUB_TYPES, BRAND, BRAND_FAMILY, GENDER, PRICE, SHAPE, MATERIAL, I18N_COLOR,
                    COLOR, TAGS);

    public static final Map<ItemType, List<IndexedField>> FILTER_MAP = new HashMap<>();
    public static final List<IndexedField> LENS_FILTERS =
            asList(USAGE_TYPES, SUB_TYPES, BRAND, BRAND_FAMILY, PRICE, TAGS);
    public static final List<IndexedField> FRAME_FILTERS =
            asList(GENDER, SUB_TYPES, BRAND, BRAND_FAMILY, PRICE, SHAPE, MATERIAL, I18N_COLOR, COLOR, TAGS);
    public static final List<IndexedField> EXTRAS_FILTERS = new ArrayList<>(SEARCH_AGGREGATIONS);
    static {
        FILTER_MAP.put(LENS, LENS_FILTERS);
        FILTER_MAP.put(FRAME, FRAME_FILTERS);
        FILTER_MAP.put(SUNWEAR, FRAME_FILTERS);
        FILTER_MAP.put(EXTRA, EXTRAS_FILTERS);
    }

    /**
     * Observe that a functional reference for the getter of these fields must be set. See {@link IndexedField#getGetterRef()}
     */
    public static final List<IndexedField> SUGGEST_FIELDS = asList(PRODUCT_NAME, BRAND, BRAND_FAMILY_RAW, I18N_TYPE);

    public static String nameFromField(String fieldValue) {
        return valueOfField(fieldValue).map(field -> field.getName()).orElse(fieldValue);
    }

    public static Optional<IndexedField> valueOfField(String fieldValue) {
        for (IndexedField indexedField : ALL) {
            if (indexedField.getField().equals(fieldValue) ||
                    indexedField.getField().equals(fieldValue.replace(KEYWORD_SUFFIX, EMPTY))) {
                return Optional.of(indexedField);
            }
        }
        return Optional.empty();
    }
}
