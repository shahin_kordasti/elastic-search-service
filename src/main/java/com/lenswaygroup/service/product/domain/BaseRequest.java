package com.lenswaygroup.service.product.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder(builderMethodName = "baseRequestBuilder")
@NoArgsConstructor
@AllArgsConstructor
public class BaseRequest {
    private Integer orgId;
    private String countryCode;
    private Map<String, List<String>> filters;
}
