package com.lenswaygroup.service.product.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.HashMap;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SuggestRequest extends BaseRequest {
    private String prefix;

    @Builder
    public SuggestRequest(Integer orgId, String countryCode, String prefix) {
        super(orgId, countryCode, new HashMap<>());
        this.prefix = prefix;
    }
}
