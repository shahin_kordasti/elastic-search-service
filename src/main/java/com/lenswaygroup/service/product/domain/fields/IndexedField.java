package com.lenswaygroup.service.product.domain.fields;

import com.lenswaygroup.service.product.domain.ProductSearch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.BucketOrder;

import java.util.function.Function;

import static com.lenswaygroup.service.product.domain.IndexedFields.KEYWORD_SUFFIX;
import static com.lenswaygroup.service.product.domain.IndexedFields.PROPERTY_MAP_PREFIX;
import static org.elasticsearch.common.unit.Fuzziness.ZERO;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class IndexedField {
    public static final float DEFAULT_FUZZY_BOOST = 1;
    public static final Integer DEFAULT_FUZZY_PREFIX = 0;

    private String field;
    private String name;
    private Float boost;
    private Object fuzziness;
    /**
     * References to the ProductSearch get method for this field. Needed in case the value need to be resolved dynamically.
     */
    private Function<ProductSearch, Object> getterRef;
    private Integer fuzzinessPrefix = DEFAULT_FUZZY_PREFIX;
    private Float fuzzinessBoost = DEFAULT_FUZZY_BOOST;

    public IndexedField(String field, String name, float boost, Object fuzziness) {
        this.field = field;
        this.name = name;
        this.boost = boost;
        this.fuzziness = fuzziness;
    }

    public IndexedField(String field, String name, float boost, Object fuzziness,
                        Function<ProductSearch, Object> getterRef) {
        this.field = field;
        this.name = name;
        this.boost = boost;
        this.fuzziness = fuzziness;
        this.getterRef = getterRef;
    }

    public IndexedField(String field, String name, float boost, Object fuzziness, float fuzzinessBoost, Function<ProductSearch, Object> getterRef) {
        this(field, name, boost, fuzziness);
        this.fuzzinessBoost = fuzzinessBoost;
        this.getterRef = getterRef;
    }

    public IndexedField(String field, String name, float boost, Object fuzziness, float fuzzinessBoost) {
        this(field, name, boost, fuzziness);
        this.fuzzinessBoost = fuzzinessBoost;
    }

    public String getField() {
        if (field.startsWith(PROPERTY_MAP_PREFIX)) {
            return field + KEYWORD_SUFFIX;
        } else {
            return field;
        }
    }

    public boolean shouldFuzzySearch() {
        return !fuzziness.equals(ZERO);
    }

    public QueryBuilder filterQuery(String filterField, String filterValue) {
        return QueryBuilders.termQuery(getField(), filterValue);
    }

    public AbstractAggregationBuilder aggregationQuery() {
        return terms(getField()).field(getField()).order(BucketOrder.count(false));
    }

    public float getFuzzyBoost() {
        return fuzzinessBoost;
    }

    public Object getFieldValue(ProductSearch productSearch) {
        if (getterRef != null) {
            return getterRef.apply(productSearch);
        } else {
            throw new UnsupportedOperationException("No getterRef is set for this IndexedField!");
        }
    }
}
