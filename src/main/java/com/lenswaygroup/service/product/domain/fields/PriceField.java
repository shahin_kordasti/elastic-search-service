package com.lenswaygroup.service.product.domain.fields;

import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.filter.FiltersAggregator;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.filters;

public class PriceField extends IndexedField {

    private static final Map<String, Function<PriceField, FiltersAggregator.KeyedFilter>> PRICE_FILTERS =
            new HashMap<>();
    public static final String LT_500 = "< 500";
    public static final String BTW_500_1000 = "500 - 1000";
    public static final String GT_500 = "> 1000";
    static {
        PRICE_FILTERS.put(LT_500, (indexedField) -> new FiltersAggregator.KeyedFilter(LT_500,
                rangeQuery(indexedField.getField()).lt(500d)));
        PRICE_FILTERS.put(BTW_500_1000, (indexedField) -> new FiltersAggregator.KeyedFilter(BTW_500_1000,
                rangeQuery(indexedField.getField()).gte(500d).lte(1000d)));
        PRICE_FILTERS.put(GT_500, (indexedField) -> new FiltersAggregator.KeyedFilter(GT_500,
                rangeQuery(indexedField.getField()).gt(1000d)));
    }

    public PriceField(String field, String name, float boost, Fuzziness fuzziness) {
        super(field, name, boost, fuzziness);
    }

    @Override
    public QueryBuilder filterQuery(String filterField, String filterValue) {
        return PRICE_FILTERS.get(filterValue).apply(this).filter();
    }

    @Override
    public AbstractAggregationBuilder aggregationQuery() {
        return filters(getField(), PRICE_FILTERS.get(LT_500).apply(this), PRICE_FILTERS.get(BTW_500_1000).apply(this),
                PRICE_FILTERS.get(GT_500).apply(this));
    }
}
