package com.lenswaygroup.service.product.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SearchIdRequest extends BaseRequest {
    private String id;

    @Builder
    public SearchIdRequest(String id, int orgId, String countryCode) {
        super(orgId, countryCode, new HashMap<>());
        this.id = id;
    }
}
