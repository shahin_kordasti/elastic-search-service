package com.lenswaygroup.service.product.domain;

public enum ItemType {
    EXTRA, FRAME, LENS, SUNWEAR
}
