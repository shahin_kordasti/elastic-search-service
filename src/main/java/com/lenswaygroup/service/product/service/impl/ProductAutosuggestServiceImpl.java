package com.lenswaygroup.service.product.service.impl;

import com.lenswaygroup.service.product.domain.SuggestRequest;
import com.lenswaygroup.service.product.domain.fields.IndexedField;
import com.lenswaygroup.service.product.service.ProductAutosuggestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.lenswaygroup.service.product.domain.IndexedFields.SUGGEST_FIELDS;
import static com.lenswaygroup.service.product.service.impl.ProductIndexingServiceImpl.productIndexName;

@RequiredArgsConstructor
@Log4j2
@Service
public class ProductAutosuggestServiceImpl implements ProductAutosuggestService {

    private static final String SUGGESTION_FIELD = "suggestions";
    public static final int MAX_SUGGEST_SIZE = 12;

    private final ElasticsearchOperations elasticsearchOp;

    @Override
    public List<String> getSuggestions(SuggestRequest suggestRequest) {
        CompletionSuggestionBuilder suggestionBuilder = SuggestBuilders.completionSuggestion(SUGGESTION_FIELD)
                .prefix(suggestRequest.getPrefix(), Fuzziness.AUTO).size(MAX_SUGGEST_SIZE).skipDuplicates(true);
        SearchResponse response = elasticsearchOp.getClient().prepareSearch()
                .setIndices(productIndexName(suggestRequest.getOrgId(), suggestRequest.getCountryCode()))
                .suggest(new SuggestBuilder().addSuggestion(SUGGESTION_FIELD, suggestionBuilder)).execute().actionGet();

        CompletionSuggestion completionSuggestion = response.getSuggest().getSuggestion(SUGGESTION_FIELD);

        return buildSuggestions(completionSuggestion);
    }

    private List<String> buildSuggestions(CompletionSuggestion completionSuggestion) {
        List<String> searchSuggestions = new ArrayList<>();
        for (CompletionSuggestion.Entry entry : completionSuggestion.getEntries()) {
            for (CompletionSuggestion.Entry.Option option : entry) {
                String matchedText = option.getText().toString();
                Map<String, Object> sourceMap = option.getHit().getSourceAsMap();
                for (IndexedField suggestField : SUGGEST_FIELDS) {
                    Object fieldValue = sourceMap.get(suggestField.getField());
                    if (shouldAddField(searchSuggestions, matchedText, fieldValue)) {
                        searchSuggestions.add(fieldValue.toString());
                    }
                }
            }
        }
        return searchSuggestions;
    }

    private boolean shouldAddField(List<String> searchSuggestions, String matchedText, Object fieldValue) {
        return fieldValueContainsMatchedText(matchedText, fieldValue) && searchSuggestions.size() < MAX_SUGGEST_SIZE;
    }

    public boolean fieldValueContainsMatchedText(String matchedText, Object fieldValue) {
        return fieldValue != null && fieldValue.toString().toLowerCase().contains(matchedText.toLowerCase());
    }
}
