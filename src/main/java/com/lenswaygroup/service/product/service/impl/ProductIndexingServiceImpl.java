package com.lenswaygroup.service.product.service.impl;

import com.carrotsearch.hppc.cursors.ObjectCursor;
import com.lenswaygroup.service.product.converter.ProductExportSearchConverter;
import com.lenswaygroup.service.product.domain.ProductSearch;
import com.lenswaygroup.service.product.domain.fields.IndexedField;
import com.lenswaygroup.service.product.integration.dto.ProductExportEventDTO;
import com.lenswaygroup.service.product.service.ProductIndexingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.MappingBuilderProxy;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.query.AliasQuery;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.lenswaygroup.service.product.domain.IndexedFields.SUGGEST_FIELDS;
import static com.lenswaygroup.service.product.util.ResourceUtil.classPathResourceToString;
import static com.lenswaygroup.service.product.util.ValueUtil.tokenize;
import static java.util.Collections.addAll;
import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
@Log4j2
@Service
public class ProductIndexingServiceImpl implements ProductIndexingService {
    public static final String PRODUCT_INDEX = "product";
    private static final String ELASTIC_SEARCH_SETTINGS = "elastic_search_settings.json";
    private static final String PRODUCT_INDEX_TYPE = "productsearch";

    private final ElasticsearchOperations elasticSearchOps;
    private final ProductExportSearchConverter prodSearchConverter;
    private final MappingBuilderProxy mappingBuilderProxy;
    private String customSettings;

    @PostConstruct
    public void loadCustomSettingsFromFile() {
        customSettings = classPathResourceToString(ELASTIC_SEARCH_SETTINGS);
    }

    @Override
    public void indexAllProducts(ProductExportEventDTO productExportEvent) {
        LOGGER.info("Indexing products...");
        String aliasName = productIndexName(productExportEvent);
        String indexSuffix = DateTimeFormatter.ofPattern("_yyyyMMdd_HH_mm_ss_SSS").format(ZonedDateTime.now());
        String indexName = aliasName + indexSuffix;
        List<ProductSearch> productsToIndex =
                productExportEvent.getProductExports().stream().map(impProd -> prodSearchConverter.toEntity(impProd))
                        .collect(toList());
        bulkIndex(aliasName, indexName, productsToIndex);
        LOGGER.info("Done indexing products!");
    }

    private synchronized void bulkIndex(String aliasName, String indexName, List<ProductSearch> productsToIndex) {
        List<IndexQuery> indexQueries = buildProductIndices(indexName, productsToIndex);
        if (indexQueries.size() > 0) {
            elasticSearchOps.createIndex(indexName, customSettings);
            elasticSearchOps.putMapping(indexName, PRODUCT_INDEX_TYPE, mappingBuilderProxy
                    .getMappings(elasticSearchOps.getPersistentEntityFor(ProductSearch.class), ProductSearch.class));
            elasticSearchOps.bulkIndex(indexQueries);
            elasticSearchOps.refresh(indexName);

            ImmutableOpenMap<String, List<AliasMetaData>> prevIndices =
                    elasticSearchOps.getClient().admin().indices().prepareGetAliases(aliasName).get().getAliases();

            addIndexToAlias(aliasName, indexName);

            deletePreviousIndices(aliasName, prevIndices);
        }
    }

    private List<IndexQuery> buildProductIndices(String indexName, List<ProductSearch> productsToIndex) {
        List<IndexQuery> indexQueries = new ArrayList<>();
        for (ProductSearch productIndex : productsToIndex) {
            List<String> productSuggestions = buildTokenizedSuggestions(productIndex);
            productIndex.setSuggestions(new Completion(productSuggestions.toArray(new String[0])));

            IndexQuery indexQuery = new IndexQueryBuilder().withId(productIndex.getId()).withObject(productIndex)
                    .withIndexName(indexName).build();
            indexQueries.add(indexQuery);
        }
        return indexQueries;
    }

    private void addIndexToAlias(String aliasName, String indexName) {
        AliasQuery aliasQuery = new AliasQuery();
        aliasQuery.setAliasName(aliasName);
        aliasQuery.setIndexName(indexName);
        elasticSearchOps.addAlias(aliasQuery);
    }

    private void deletePreviousIndices(String aliasName, ImmutableOpenMap<String, List<AliasMetaData>> prevIndexes) {
        for (ObjectCursor<String> indexCursor : prevIndexes.keys()) {
            AliasQuery aliasQuery = new AliasQuery();
            aliasQuery.setAliasName(aliasName);
            aliasQuery.setIndexName(indexCursor.value);
            elasticSearchOps.removeAlias(aliasQuery);
            elasticSearchOps.deleteIndex(indexCursor.value);
        }
    }

    /**
     * See {@link com.lenswaygroup.service.product.domain.IndexedFields#SUGGEST_FIELDS} for which fields to be used for
     * autocomplete/suggestion
     */
    private List<String> buildTokenizedSuggestions(ProductSearch productIndex) {
        List<String> productSuggestions = new ArrayList<>();
        for (IndexedField suggestField : SUGGEST_FIELDS) {
            addAll(productSuggestions, tokenize((String) suggestField.getGetterRef().apply(productIndex)));
        }
        productSuggestions.removeIf(Objects::isNull);
        return productSuggestions;
    }

    public static String productIndexName(Integer orgId, String countryCode) {
        return (PRODUCT_INDEX + "-" + orgId + countryCode).toLowerCase();
    }

    public static String productIndexName(ProductExportEventDTO productExportEvent) {
        return productIndexName(productExportEvent.getOrgId(), productExportEvent.getCountryCode());
    }
}
