package com.lenswaygroup.service.product.service;

import com.lenswaygroup.service.product.domain.SuggestRequest;

import java.util.List;

public interface ProductAutosuggestService {

    List<String> getSuggestions(SuggestRequest suggestRequest);
}
