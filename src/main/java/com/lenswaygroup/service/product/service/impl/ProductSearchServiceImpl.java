package com.lenswaygroup.service.product.service.impl;

import com.lenswaygroup.service.product.converter.aggregation.AggregationConverter;
import com.lenswaygroup.service.product.domain.AggregationContainer;
import com.lenswaygroup.service.product.domain.BaseRequest;
import com.lenswaygroup.service.product.domain.FilterRequest;
import com.lenswaygroup.service.product.domain.IndexedFields;
import com.lenswaygroup.service.product.domain.ProductSearch;
import com.lenswaygroup.service.product.domain.ProductSearchResult;
import com.lenswaygroup.service.product.domain.SearchIdRequest;
import com.lenswaygroup.service.product.domain.SearchTextRequest;
import com.lenswaygroup.service.product.domain.fields.IndexedField;
import com.lenswaygroup.service.product.service.ProductSearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.lenswaygroup.service.product.domain.IndexedFields.FILTER_MAP;
import static com.lenswaygroup.service.product.domain.IndexedFields.ITEM_TYPE;
import static com.lenswaygroup.service.product.domain.IndexedFields.SEARCH_AGGREGATIONS;
import static com.lenswaygroup.service.product.domain.IndexedFields.SEARCH_FIELDS;
import static com.lenswaygroup.service.product.service.impl.ProductIndexingServiceImpl.productIndexName;
import static java.lang.String.format;
import static org.elasticsearch.common.unit.Fuzziness.ZERO;
import static org.elasticsearch.index.query.Operator.AND;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

@RequiredArgsConstructor
@Log4j2
@Service
public class ProductSearchServiceImpl implements ProductSearchService {
    public static final int MAX_SEARCH_HITS = 1000;

    private final ElasticsearchOperations elasticsearchOp;
    private final AggregationConverter aggrConverter;

    @Override
    public ProductSearchResult search(SearchTextRequest searchRequest) {
        LOGGER.info("Searching products using: " + searchRequest);

        QueryBuilder searchQueryBuilder = searchQuery(searchRequest.getSearchText());
        return searchAndFilter(searchRequest, searchQueryBuilder, SEARCH_AGGREGATIONS);
    }

    @Override
    public ProductSearchResult filter(FilterRequest filtersReq) {
        LOGGER.info("Filtering products using: " + filtersReq);

        TermQueryBuilder filterQueryBuilder = termQuery(ITEM_TYPE.getField(), filtersReq.getItemType().toString());
        return searchAndFilter(filtersReq, filterQueryBuilder, FILTER_MAP.get(filtersReq.getItemType()));
    }

    private ProductSearchResult searchAndFilter(BaseRequest searchRequest, QueryBuilder queryBuilder,
                                                List<IndexedField> aggregatedFields) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder().withQuery(queryBuilder)
                .withFilter(addFilters(searchRequest.getFilters())).withPageable(PageRequest.of(0, MAX_SEARCH_HITS))
                .withIndices(productIndexName(searchRequest.getOrgId(), searchRequest.getCountryCode()));

        searchQueryBuilder = addAggregators(searchQueryBuilder, aggregatedFields);

        //TODO if needed for performance, find a way to combine these two queries into one
        SearchQuery searchQuery = searchQueryBuilder.build();
        List<ProductSearch> hits = elasticsearchOp.queryForList(searchQuery, ProductSearch.class);
        List<AggregationContainer> aggregations = getAggregations(aggregatedFields, searchQuery);
        stopWatch.stop();
        LOGGER.info("Done Search/Filtering products. Got [{}] hits taking [{}] ms", hits.size(), stopWatch.getTime());

        return ProductSearchResult.builder().hits(hits).took(stopWatch.getTime()).aggregations(aggregations).build();
    }

    private List<AggregationContainer> getAggregations(List<IndexedField> aggregatedFields, SearchQuery searchQuery) {
        List<AggregationContainer> aggregations = elasticsearchOp
                .query(searchQuery, response -> aggrConverter.toAggregations(response.getAggregations()));
        aggregations.sort((AggregationContainer o1, AggregationContainer o2) -> {
            Integer sortOrder1 = getAggrSortOrder(o1.getField(), aggregatedFields);
            Integer sortOrder2 = getAggrSortOrder(o2.getField(), aggregatedFields);
            return sortOrder1.compareTo(sortOrder2);
        });
        return aggregations;
    }

    private Integer getAggrSortOrder(String field, List<IndexedField> aggregatedFields) {
        Integer sortOrder1 = 0;
        Optional<IndexedField> indexedField1 = IndexedFields.valueOfField(field);
        if (indexedField1.isPresent()) {
            sortOrder1 = aggregatedFields.indexOf(indexedField1.get());
        }
        return sortOrder1;
    }

    private NativeSearchQueryBuilder addAggregators(NativeSearchQueryBuilder queryBuilder,
                                                    List<IndexedField> aggregatedFields) {
        for (IndexedField aggrField : aggregatedFields) {
            queryBuilder = queryBuilder.addAggregation(aggrField.aggregationQuery());
        }
        return queryBuilder;
    }

    private BoolQueryBuilder searchQuery(String searchText) {
        BoolQueryBuilder boolQuery = boolQuery();
        for (IndexedField searchField : SEARCH_FIELDS) {
            boolQuery = boolQuery.should(match(searchText, searchField.getField(), searchField.getBoost(), ZERO, 0));
            if (searchField.shouldFuzzySearch()) {
                boolQuery.should(match(searchText, searchField.getField(), searchField.getFuzzyBoost(),
                        searchField.getFuzziness(), searchField.getFuzzinessPrefix()));
            }
        }
        return boolQuery;
    }

    private MatchQueryBuilder match(String searchText, String field, float boost, Object fuzziness, int prefixLength) {
        return matchQuery(field, searchText).boost(boost).operator(AND).fuzziness(fuzziness).prefixLength(prefixLength);
    }

    private BoolQueryBuilder addFilters(Map<String, List<String>> filters) {
        BoolQueryBuilder boolQuery = boolQuery();
        if (MapUtils.isNotEmpty(filters)) {
            for (String filterField : filters.keySet()) {
                Optional<IndexedField> fieldOptional = IndexedFields.valueOfField(filterField);
                if (fieldOptional.isPresent()) {
                    BoolQueryBuilder filterGroup = boolQuery();
                    for (String filterValue : filters.get(filterField)) {
                        filterGroup = filterGroup.should(fieldOptional.get().filterQuery(filterField, filterValue));
                    }
                    boolQuery.filter(filterGroup);
                }
            }
        }
        return boolQuery;
    }

    @Override
    public ProductSearchResult findBy(SearchIdRequest searchIdReq) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        NativeSearchQuery searchQuery =
                new NativeSearchQueryBuilder().withQuery(QueryBuilders.idsQuery().addIds(searchIdReq.getId()))
                        .withIndices(productIndexName(searchIdReq.getOrgId(), searchIdReq.getCountryCode())).build();
        List<ProductSearch> productSearchHits = elasticsearchOp.queryForList(searchQuery, ProductSearch.class);
        if (productSearchHits.size() == 1) {
            return ProductSearchResult.builder().hits(productSearchHits).took(stopWatch.getTime()).build();
        } else {
            throw new RuntimeException(
                    format("Unexpected result returned when finding product by {%s}. Received: {%s} ", searchIdReq,
                            productSearchHits));
        }
    }
}
