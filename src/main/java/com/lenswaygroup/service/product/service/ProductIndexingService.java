package com.lenswaygroup.service.product.service;

import com.lenswaygroup.service.product.integration.dto.ProductExportEventDTO;

public interface ProductIndexingService {

    void indexAllProducts(ProductExportEventDTO importedProducts);
}
