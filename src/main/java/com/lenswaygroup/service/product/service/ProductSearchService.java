package com.lenswaygroup.service.product.service;

import com.lenswaygroup.service.product.domain.FilterRequest;
import com.lenswaygroup.service.product.domain.ProductSearchResult;
import com.lenswaygroup.service.product.domain.SearchIdRequest;
import com.lenswaygroup.service.product.domain.SearchTextRequest;

public interface ProductSearchService {
    ProductSearchResult search(SearchTextRequest searchRequest);

    ProductSearchResult filter(FilterRequest filtersReq);

    ProductSearchResult findBy(SearchIdRequest searchIdReq);
}
