package com.lenswaygroup.service.product.util;

import com.lenswaygroup.service.product.integration.dto.ProductExportDTO;
import com.lenswaygroup.service.product.integration.dto.ProductExportEventDTO;
import com.lenswaygroup.service.product.integration.dto.ProductPropertyValueDTO;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomUtils.nextDouble;
import static org.apache.commons.lang3.RandomUtils.nextLong;

public class TestDataUtil {

    public static ProductExportEventDTO generateProdExportEvent(String countryCode, int orgId) {
        List<ProductExportDTO> prods = asList(generateProdExportDTO());
        return ProductExportEventDTO.builder().countryCode(countryCode).orgId(orgId).productExports(prods).build();
    }

    public static ProductExportDTO generateProdExportDTO() {
        List<ProductPropertyValueDTO> propVals =
                new ArrayList<>(asList(ProductPropertyValueDTO.builder().id(nextLong(1, 1111)).name(randomAlphanumeric(1, 1111))
                        .value(randomAlphanumeric(1, 11111)).build()));
        return ProductExportDTO.builder().id(randomAlphanumeric(1, 1111)).brand(randomAlphanumeric(1, 1111))
                .productName(randomAlphanumeric(1, 1111)).propertyValues(propVals)
                .listImageUrl(generateUrl().toString()).originalImageUrl(generateUrl().toString())
                .i18nType(randomAlphabetic(4, 10)).price(nextDouble(1,1111)).productgroupId(nextLong(1,1111)).build();
    }

    public static URL generateUrl() {
        try {
            return new URL("http", randomAlphabetic(5, 10) + "." + randomAlphabetic(2, 3) + "/",
                    randomAlphabetic(10, 50));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
