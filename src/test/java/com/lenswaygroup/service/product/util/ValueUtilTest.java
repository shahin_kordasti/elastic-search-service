package com.lenswaygroup.service.product.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ValueUtilTest {

    @Test
    public void shouldTokenizeSingleWordValue(){
        //Given
        String fieldValue = "Ray-ban";
        //When
        String[] tokenizedFieldValue = ValueUtil.tokenize(fieldValue);
        //Then
        assertEquals(1, tokenizedFieldValue.length);
        assertEquals(fieldValue, tokenizedFieldValue[0]);
    }

    @Test
    public void shouldTokenizeMultiWordValue(){
        //Given
        String fieldValue = "1-day Acuvue Moist";
        //When
        String[] tokenizedFieldValue = ValueUtil.tokenize(fieldValue);
        //Then
        assertEquals(3, tokenizedFieldValue.length);
        assertEquals(fieldValue, tokenizedFieldValue[0]);
        assertEquals("Acuvue Moist", tokenizedFieldValue[1]);
        assertEquals("Moist", tokenizedFieldValue[2]);
    }

    @Test
    public void shouldNotTokenizeBlankString(){
        //Given
        String fieldValue = " ";
        //When
        String[] tokenizedFieldValue = ValueUtil.tokenize(fieldValue);
        //Then
        assertEquals(0, tokenizedFieldValue.length);
    }
}