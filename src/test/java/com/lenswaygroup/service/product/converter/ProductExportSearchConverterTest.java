package com.lenswaygroup.service.product.converter;

import com.lenswaygroup.service.product.domain.ProductSearch;
import com.lenswaygroup.service.product.integration.dto.ProductExportDTO;
import com.lenswaygroup.service.product.integration.dto.ProductPropertyValueDTO;
import com.lenswaygroup.service.product.integration.dto.SubTypesDTO;
import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import static com.lenswaygroup.service.product.converter.ProductExportSearchConverter.TAGS_PROP_NAME;
import static com.lenswaygroup.service.product.converter.ProductExportSearchConverter.USAGE_TYPE_IDS;
import static com.lenswaygroup.service.product.util.TestDataUtil.generateProdExportDTO;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class ProductExportSearchConverterTest {

    private ProductExportSearchConverter productExportSearchConverter;

    @Before
    public void setup() {
        productExportSearchConverter = new ProductExportSearchConverter(new ModelMapper());
    }

    @Test
    public void shouldMapDTOToEntity() {
        //Given
        ProductExportDTO prodExportDTO = generateProdExportDTO();
        String propVal = randomAlphanumeric(1, 111);
        prodExportDTO.getPropertyValues()
                .add(ProductPropertyValueDTO.builder().name("propName").value(propVal).build());
        prodExportDTO.getPropertyValues()
                .add(ProductPropertyValueDTO.builder().name(TAGS_PROP_NAME).value("tag1,tag2, tag3 ").build());

        String subType1 = randomAlphanumeric(1, 15);
        String subType2 = randomAlphanumeric(1, 15);
        prodExportDTO.setSubTypes(SubTypesDTO.builder().ids(asList(nextInt(), USAGE_TYPE_IDS.get(0)))
                .i18nValues(asList(subType1, subType2)).build());
        //When
        ProductSearch mappedEntity = productExportSearchConverter.toEntity(prodExportDTO);
        //Then
        assertEquals(prodExportDTO.getId(), mappedEntity.getId());
        assertEquals(prodExportDTO.getBrand(), mappedEntity.getBrand());
        assertEquals(prodExportDTO.getProductName(), mappedEntity.getProductName());
        assertEquals(prodExportDTO.getPrice(), mappedEntity.getPrice());
        assertEquals(prodExportDTO.getI18nType(), mappedEntity.getI18nType());
        assertEquals(prodExportDTO.getType(), mappedEntity.getType());
        assertEquals(prodExportDTO.getI18nType(), mappedEntity.getI18nType());
        assertEquals(prodExportDTO.getProductgroupId(), mappedEntity.getProductgroupId());
        assertEquals(prodExportDTO.getListImageUrl(), mappedEntity.getListImageUrl());
        assertEquals(prodExportDTO.getOriginalImageUrl(), mappedEntity.getOriginalImageUrl());
        assertThat(mappedEntity.getSubTypes(), containsInAnyOrder(subType1));
        assertThat(mappedEntity.getUsageTypes(), containsInAnyOrder(subType2));
        assertEquals(3, mappedEntity.getPropertyMap().keySet().size());
        assertEquals(propVal, mappedEntity.getPropertyMap().get("propName"));
        assertThat(mappedEntity.getTags(), containsInAnyOrder("tag1", "tag2", "tag3"));
    }
}