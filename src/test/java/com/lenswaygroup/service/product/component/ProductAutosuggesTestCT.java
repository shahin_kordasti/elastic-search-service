package com.lenswaygroup.service.product.component;

import com.lenswaygroup.service.product.controller.dto.SuggestRequestDTO;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;
import java.util.List;

import static com.lenswaygroup.service.product.service.impl.ProductAutosuggestServiceImpl.MAX_SUGGEST_SIZE;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductAutosuggesTestCT extends ComponentTestBase {

    @Test
    public void shouldSuggestBrandFamilyNameAndProductName() throws Exception {
        //Given indexing products with suggestions
        // When
        String prefix = "acuv";
        String productName = "Acuvue Advance for Astigmatism";
        String brandFamilyName = "Acuvue";

        ResultActions result = mockMvc.perform(post("/product/suggestions").content(objectMapper
                .writeValueAsString(SuggestRequestDTO.builder().orgId(83).countryCode("SE").prefix(prefix).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$[*]").isArray());
        result.andExpect(jsonPath("$[*]").isNotEmpty());
        result.andExpect(jsonPath("$[*]", hasSize(MAX_SUGGEST_SIZE)));

        List<String> list = Arrays.asList(objectMapper.readValue(result.andReturn().getResponse().getContentAsString(), String[].class));
        assertTrue(list.contains(productName));
        assertTrue(list.contains(brandFamilyName));
    }

    @Test
    public void shouldSuggestDifferentMatchAccordingToPrefixFuzzy() throws Exception {
        //Given indexing products with suggestions
        // When
        String prefix = "acu";
        ResultActions result = mockMvc.perform(post("/product/suggestions").content(objectMapper
                .writeValueAsString(SuggestRequestDTO.builder().orgId(83).countryCode("SE").prefix(prefix).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$[*]").isArray());
        result.andExpect(jsonPath("$[*]").isNotEmpty());
        result.andExpect(jsonPath("$[*]", hasSize(MAX_SUGGEST_SIZE)));

        String suggestions = result.andReturn().getResponse().getContentAsString();
        assertTrue(suggestions.contains("Acuvue"));
        assertTrue(suggestions.contains("Act"));
    }

    @Test
    public void shouldNotSuggestProductName() throws Exception {
        //Given indexing products with suggestions
        // When
        String prefix = "NonExist";
        ResultActions result = mockMvc.perform(post("/product/suggestions").content(objectMapper
                .writeValueAsString(SuggestRequestDTO.builder().orgId(83).countryCode("SE").prefix(prefix).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$[*]").isEmpty());
    }

    @Test
    public void shouldPartialMatchSuggestInsideOfString() throws Exception {
        //Given indexing products with suggestions
        // When
        String suggestText = "Moist";
        ResultActions result = mockMvc.perform(post("/product/suggestions").content(objectMapper
                .writeValueAsString(SuggestRequestDTO.builder().orgId(83).countryCode("SE").prefix(suggestText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().isOk());
        String[] suggestResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), String[].class);
        assertTrue(suggestResult.length > 0);
        assertEquals("1-Day Acuvue Moist", suggestResult[0]);
    }
}
