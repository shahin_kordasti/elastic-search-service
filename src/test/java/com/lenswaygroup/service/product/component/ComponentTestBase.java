package com.lenswaygroup.service.product.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lenswaygroup.service.product.ProductServiceApplication;
import com.lenswaygroup.service.product.config.ElasticStubIndexer;
import com.lenswaygroup.test.util.JsonArraySizeLimiter;
import com.lenswaygroup.test.util.WaitUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;

import static com.lenswaygroup.service.product.service.impl.ProductIndexingServiceImpl.productIndexName;
import static com.lenswaygroup.test.util.WaitUtil.waitFor;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

/**
 * All component/integration tests should sub-class this class
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = ProductServiceApplication.class)
@ActiveProfiles("component-test")
@AutoConfigureMockMvc
@Ignore("Not runnable test class")
@Log4j2
public class ComponentTestBase {
    public static final String APPLICATION_JSON_CHARSET_UTF_8 = String.valueOf(MediaType.APPLICATION_JSON_UTF8);
    public static final int DOC_MAX_ARRAY_SIZE = 3;
    public static final int MIN_INDEXED_PRODUCTS = 735;
    /**
     * Override to use other than default value
     */
    protected int restDocsMaxArraySize = DOC_MAX_ARRAY_SIZE;

    @Rule
    public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    private HealthIndicator elasticsearchHealthIndicator;
    @Autowired
    private HealthIndicator rabbitHealthIndicator;
    @Autowired
    private ElasticStubIndexer elasticStubConfiguration;
    @Autowired
    protected ElasticsearchOperations elasticsearchOp;

    private static boolean hasBeenIndexed = false;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                                 .apply(documentationConfiguration(this.restDocumentation)).alwaysDo(
                        document("{method-name}", preprocessRequest(prettyPrint()),
                                preprocessResponse(new JsonArraySizeLimiter(restDocsMaxArraySize, objectMapper),
                                        prettyPrint()))).build();
    }

    @PostConstruct
    public void checkServicesRunning() throws Exception {
        waitFor(30000, () -> {
            assertEquals("ElasticSearch server is not up", "UP",
                    elasticsearchHealthIndicator.health().getStatus().getCode());
            return null;
        });
        waitFor(20000, () -> {
            assertEquals("RabbitMQ server is not up", "UP", rabbitHealthIndicator.health().getStatus().getCode());
            return null;
        });
        if (hasBeenIndexed == false) {
            elasticStubConfiguration.indexStubData();
            hasBeenIndexed = indexExists();
        }
    }

    private Boolean indexExists() {
        return WaitUtil.waitFor(10000, () -> {
            WaitUtil.sleep(500);
            assertTrue(elasticsearchOp.count(new NativeSearchQueryBuilder().withIndices(productIndexName(83, "SE"))
                                                                           .withQuery(matchAllQuery()).build()) >
                    MIN_INDEXED_PRODUCTS);
            return true;
        }, Throwable.class);
    }
}
