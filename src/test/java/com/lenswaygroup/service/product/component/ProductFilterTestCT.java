package com.lenswaygroup.service.product.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lenswaygroup.service.product.controller.dto.FilterRequestDTO;
import com.lenswaygroup.service.product.controller.dto.ProductSearchResultDTO;
import com.lenswaygroup.service.product.domain.ItemType;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.ResultActions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.lenswaygroup.service.product.domain.IndexedFields.BRAND;
import static com.lenswaygroup.service.product.domain.IndexedFields.BRAND_FAMILY;
import static com.lenswaygroup.service.product.domain.IndexedFields.FILTER_MAP;
import static com.lenswaygroup.service.product.domain.IndexedFields.USAGE_TYPES;
import static com.lenswaygroup.service.product.domain.ItemType.FRAME;
import static com.lenswaygroup.service.product.domain.ItemType.LENS;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductFilterTestCT extends ComponentTestBase {
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldGetFiltersForLenses() throws Exception {
        //Given indexed products
        ItemType filterType = LENS;
        //When
        ResultActions result = mockMvc.perform(post("/product/filters").content(objectMapper
                .writeValueAsString(FilterRequestDTO.builder().orgId(83).countryCode("SE").type(filterType).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO allFiltersResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(allFiltersResult.getAggregations().size() > 0);
        allFiltersResult.getAggregations().forEach(filter -> assertTrue("Unexpected filter returned: " + filter,
                FILTER_MAP.get(filterType).stream()
                        .anyMatch(expFilter -> expFilter.getField().equals(filter.getField()))));
    }

    @Test
    public void shouldFilterOnLenses() throws Exception {
        //Given indexed products
        ItemType filterType = LENS;
        Map<String, List<String>> filters = new HashMap<>();
        filters.put(BRAND_FAMILY.getField(), asList("Acuvue"));
        filters.put(USAGE_TYPES.getField(), asList("Endagslinser"));
        //When
        ResultActions result = mockMvc.perform(post("/product/filters").content(objectMapper.writeValueAsString(
                FilterRequestDTO.builder().orgId(83).countryCode("SE").type(filterType).filters(filters).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO allFiltersResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(allFiltersResult.getHits().size() > 0);
        assertTrue(allFiltersResult.getAggregations().size() > 0);
        allFiltersResult.getAggregations().forEach(filter -> assertTrue("Unexpected filter returned: " + filter,
                FILTER_MAP.get(filterType).stream()
                        .anyMatch(expFilter -> expFilter.getField().equals(filter.getField()))));
        allFiltersResult.getHits().forEach(filterHit -> {
            assertEquals("Acuvue", filterHit.getBrandFamilyName());
            assertThat(filterHit.getUsageTypes(), Matchers.contains("Endagslinser"));
        });
    }

    @Test
    public void shouldFilterOnFramesInSamePropertyGroup() throws Exception {
        //Given indexed products
        ItemType filterType = FRAME;
        Map<String, List<String>> filters = new HashMap<>();
        filters.put(BRAND.getField(), asList("The Collection", "Polo Ralph Lauren"));
        //When
        ResultActions result = mockMvc.perform(post("/product/filters").content(objectMapper.writeValueAsString(
                FilterRequestDTO.builder().orgId(83).countryCode("SE").type(filterType).filters(filters).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO allFiltersResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(allFiltersResult.getHits().size() > 0);
        assertTrue(allFiltersResult.getAggregations().size() > 0);
        allFiltersResult.getAggregations().forEach(filter -> assertTrue("Unexpected filter returned: " + filter,
                FILTER_MAP.get(filterType).stream()
                        .anyMatch(expFilter -> expFilter.getField().equals(filter.getField()))));
        allFiltersResult.getHits().forEach(filterHit -> {
            assertTrue(
                    "The Collection".equals(filterHit.getBrand()) || "Polo Ralph Lauren".equals(filterHit.getBrand()));
        });
    }
}
