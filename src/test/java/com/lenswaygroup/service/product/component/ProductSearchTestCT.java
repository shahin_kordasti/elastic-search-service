package com.lenswaygroup.service.product.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lenswaygroup.service.product.controller.dto.ProductSearchDTO;
import com.lenswaygroup.service.product.controller.dto.ProductSearchResultDTO;
import com.lenswaygroup.service.product.controller.dto.SearchRequestDTO;
import com.lenswaygroup.service.product.domain.AggregationBucket;
import com.lenswaygroup.service.product.domain.IndexedFields;
import com.lenswaygroup.service.product.domain.fields.IndexedField;
import com.lenswaygroup.service.product.domain.fields.PriceField;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.lenswaygroup.service.product.domain.IndexedFields.BRAND;
import static com.lenswaygroup.service.product.domain.IndexedFields.BRAND_FAMILY;
import static com.lenswaygroup.service.product.domain.IndexedFields.I18N_TYPE;
import static com.lenswaygroup.service.product.domain.IndexedFields.PRICE;
import static com.lenswaygroup.service.product.domain.IndexedFields.SHAPE;
import static com.lenswaygroup.service.product.domain.IndexedFields.SUB_TYPES;
import static com.lenswaygroup.service.product.domain.IndexedFields.TAGS;
import static com.lenswaygroup.service.product.domain.IndexedFields.USAGE_TYPES;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductSearchTestCT extends ComponentTestBase {
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldMatchAllGlasses() throws Exception {
        //Given indexed products
        //When
        String searchText = "glasögon";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 200);
        //check top hits are frames
        for (int i = 0; i < 150; i++) {
            ProductSearchDTO prod = searchResult.getHits().get(i);
            assertTrue("Hits at " + i + " was not a frame", prod.getType().equalsIgnoreCase("FRAME") ||
                    prod.getProductName().equals("Synundersökning Glasögon"));
        }
    }

    @Test
    public void shouldMatchOnLensSubType() throws Exception {
        //Given indexed products
        //When
        String searchText = "Veckolinser";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 0);
        boolean allLenses = searchResult.getHits().stream().allMatch(prod -> prod.getType().equalsIgnoreCase("LENS"));
        assertTrue(allLenses);
        boolean allWeeklyLenses =
                searchResult.getHits().stream().allMatch(prod -> prod.getUsageTypes().contains("Veckolinser"));
        assertTrue(allWeeklyLenses);
    }

    @Test
    public void shouldMatchNoProducts() throws Exception {
        //Given indexed products
        //When
        String searchText = "abcdefghjklmnopqrstuvwxyz";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertEquals(0, searchResult.getHits().size());
    }

    @Test
    public void shouldPartialMatchOnProductName() throws Exception {
        //Given indexed products
        //When
        String searchText = "Acuvu";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        List<ProductSearchDTO> productSearchDTOs = searchResult.getHits();
        assertTrue(productSearchDTOs.size() > 0);
        assertTrue(productSearchDTOs.stream().anyMatch(prod -> prod.getProductName().contains("Acuvue")));
    }

    @Test
    public void shouldMatchOnProductBrandContainingHyphen() throws Exception {
        //Given indexed products
        //When
        String searchText = "ray-ban";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        List<ProductSearchDTO> productSearchDTOs = searchResult.getHits();
        assertTrue(productSearchDTOs.size() > 0);
        //checking scoring
        boolean allRayBan = productSearchDTOs.stream().allMatch(
                prod -> prod.getBrand().equalsIgnoreCase(searchText) || prod.getBrand().equals("Ray-Ban Junior"));
        assertTrue(allRayBan);
    }

    @Test
    public void shouldMatchOnShape() throws Exception {
        //Given indexed products
        //When
        String searchText = "rektangulär";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 0);
        boolean allGlasses = searchResult.getHits().stream().allMatch(
                prod -> prod.getType().equalsIgnoreCase("FRAME") || prod.getType().equalsIgnoreCase("SUNWEAR"));
        assertTrue(allGlasses);
        boolean allRectangular = searchResult.getHits().stream()
                .allMatch(prod -> "Rektangulär".equalsIgnoreCase(prod.getPropertyMap().get("shape")));
        assertTrue(allRectangular);
    }

    @Test
    public void shouldMatchOnColor() throws Exception {
        //Given indexed products
        //When
        String searchText = "grön";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 15);
        //check top hits are grön
        for (int i = 0; i < 15; i++) {
            ProductSearchDTO prod = searchResult.getHits().get(i);
            assertTrue("Hits at " + i + " was not grön", "grön".equalsIgnoreCase(prod.getColors()));
        }
    }

    @Test
    public void shouldNgramMatch() throws Exception {
        //Given indexed products
        //When
        String searchText = "Endagsl";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 0);
        //check top hits are endagslinser
        boolean allEndagsLinser = searchResult.getHits().stream()
                .allMatch(prod -> prod.getPropertyMap().get("type").contains("Endagslinser"));
        assertTrue(allEndagsLinser);
    }

    @Test
    public void shouldMatchOnMisspelledTerms() throws Exception {
        //Given indexed products
        //When
        String searchText = "Dayli";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 10);
        //check top hits are DAILIES
        for (int i = 0; i < 10; i++) {
            ProductSearchDTO prod = searchResult.getHits().get(i);
            assertTrue("Hits at " + i + " was not DAILIES", "DAILIES".equalsIgnoreCase(prod.getBrandFamilyName()));
        }
    }

    @Test
    public void shouldMatchOnTag() throws Exception {
        //Given indexed products
        //When
        String searchText = "OFFBEAT";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 0);
        searchResult.getHits().forEach(prod -> assertThat(prod.getTags(), hasItem(searchText)));
    }

    @Test
    public void shouldAggregateSearchResult() throws Exception {
        //Given indexed products
        //When
        String searchText = "svart";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 0);
        assertTrue(searchResult.getAggregations().size() > 0);
        assertTrue(hasItemTypeAggrAndCountEqualsTotalHits(searchResult));
        //not all products have all aggregations so removing the ones that are missing
        List<IndexedField> expectedAggregations = new ArrayList<>(IndexedFields.SEARCH_AGGREGATIONS);
        expectedAggregations.remove(BRAND_FAMILY);
        expectedAggregations.remove(USAGE_TYPES);
        expectedAggregations.remove(SUB_TYPES);
        expectedAggregations.remove(TAGS);

        expectedAggregations.forEach(searchAggr -> assertTrue("Could not find aggregation: " + searchAggr,
                hasAggregation(searchResult, searchAggr.getField())));
    }

    @Test
    public void shouldFilterOnSearchResults() throws Exception {
        //Given indexed products
        Map<String, List<String>> filters = new HashMap<>();
        filters.put(BRAND.getField(), asList("The Collection"));
        filters.put(SHAPE.getField(), asList("Wayfarer"));
        //When
        String searchText = "svart";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).filters(filters).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 0);
        assertTrue(searchResult.getAggregations().size() > 0);
        boolean allOfFilteredBrand =
                searchResult.getHits().stream().allMatch(prod -> "The Collection".equals(prod.getBrand()));
        assertTrue(allOfFilteredBrand);
        boolean allOfFilteredShape =
                searchResult.getHits().stream().allMatch(prod -> "Wayfarer".equals(prod.getPropertyMap().get("shape")));
        assertTrue(allOfFilteredShape);
    }

    @Test
    public void shouldFilterSearchResultsOnPrice() throws Exception {
        //Given indexed products
        MultiValueMap<String, String> filters = new LinkedMultiValueMap<>();
        filters.put(PRICE.getField(), asList(PriceField.BTW_500_1000));
        //When
        String searchText = "linser";
        ResultActions result = mockMvc.perform(post("/product/search").content(objectMapper.writeValueAsString(
                SearchRequestDTO.builder().orgId(83).countryCode("SE").searchText(searchText).filters(filters).build()))
                .contentType(APPLICATION_JSON_CHARSET_UTF_8));
        //Then
        result.andExpect(status().is(200));
        ProductSearchResultDTO searchResult = objectMapper
                .readValue(result.andReturn().getResponse().getContentAsString(), ProductSearchResultDTO.class);
        assertTrue(searchResult.getHits().size() > 0);
        boolean allWithinPriceFilter =
                searchResult.getHits().stream().allMatch(prod -> prod.getPrice() >= 500 && prod.getPrice() <= 1000);
        assertTrue(allWithinPriceFilter);
    }

    private boolean hasAggregation(ProductSearchResultDTO searchResult, String aggregation) {
        return searchResult.getAggregations().stream().anyMatch(aggr -> aggr.getField().equals(aggregation));
    }

    private boolean hasItemTypeAggrAndCountEqualsTotalHits(ProductSearchResultDTO searchResult) {
        return searchResult.getAggregations().stream().anyMatch(
                aggr -> aggr.getField().equals(I18N_TYPE.getField()) && CollectionUtils.isNotEmpty(aggr.getBuckets()) &&
                        sumBucketCount(aggr.getBuckets()) == searchResult.getHits().size());
    }

    private long sumBucketCount(List<AggregationBucket> buckets) {
        return buckets.stream().mapToLong(bucket -> bucket.getCount()).sum();
    }
}
