package com.lenswaygroup.service.product.service.impl;

import com.lenswaygroup.service.product.converter.ProductExportSearchConverter;
import com.lenswaygroup.service.product.integration.dto.ProductExportEventDTO;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequestBuilder;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesResponse;
import org.elasticsearch.client.AdminClient;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.MappingBuilderProxy;

import java.util.ArrayList;
import java.util.List;

import static com.lenswaygroup.service.product.util.TestDataUtil.generateProdExportEvent;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductIndexingServiceESTest {

    @Mock
    private ElasticsearchOperations elasticSearchOpsMock;
    @Mock
    private MappingBuilderProxy mappingBuilderProxy;

    private ProductIndexingServiceImpl indexingService;

    @Before
    public void setup() {
        indexingService = new ProductIndexingServiceImpl(elasticSearchOpsMock,
                new ProductExportSearchConverter(new ModelMapper()), mappingBuilderProxy);
        indexingService.loadCustomSettingsFromFile();

        Client clientMock = mock(Client.class);
        AdminClient adminMock = mock(AdminClient.class);
        IndicesAdminClient indicesClient = mock(IndicesAdminClient.class);
        GetAliasesRequestBuilder aliasBuilder = mock(GetAliasesRequestBuilder.class);
        GetAliasesResponse aliasResp = mock(GetAliasesResponse.class);
        ImmutableOpenMap<String, List<AliasMetaData>> aliases =
                ImmutableOpenMap.<String, List<AliasMetaData>>builder().build();
        when(aliasResp.getAliases()).thenReturn(aliases);
        when(aliasBuilder.get()).thenReturn(aliasResp);
        when(indicesClient.prepareGetAliases(Mockito.anyString())).thenReturn(aliasBuilder);
        when(adminMock.indices()).thenReturn(indicesClient);
        when(clientMock.admin()).thenReturn(adminMock);
        when(elasticSearchOpsMock.getClient()).thenReturn(clientMock);
    }

    @Test
    public void shouldIndexProducts() {
        //Given
        ProductExportEventDTO importedProducts = generateProdExportEvent("SE", 83);

        //When
        indexingService.indexAllProducts(importedProducts);
        //Then
        verify(elasticSearchOpsMock).createIndex(contains(expectedIndexPrefix()), any(String.class));
        verify(elasticSearchOpsMock).putMapping(contains(expectedIndexPrefix()), eq("productsearch"), any());
        verify(elasticSearchOpsMock).bulkIndex(any());
        verify(elasticSearchOpsMock).refresh(contains(expectedIndexPrefix()));
        verify(elasticSearchOpsMock).addAlias(any());
    }

    @Test
    public void shouldNotIndexProductsIfReceivedEmptyListOfProds() {
        //Given
        ProductExportEventDTO importedProducts = generateProdExportEvent("SE", 83);
        importedProducts.setProductExports(new ArrayList<>());

        //When
        indexingService.indexAllProducts(importedProducts);
        //Then
        verify(elasticSearchOpsMock, never()).createIndex(any(String.class), any(String.class));
        verify(elasticSearchOpsMock, never()).putMapping(eq(expectedIndexPrefix()), eq("productsearch"), any());
        verify(elasticSearchOpsMock, never()).bulkIndex(any());
        verify(elasticSearchOpsMock, never()).refresh(expectedIndexPrefix());
        verify(elasticSearchOpsMock, never()).addAlias(any());
    }

    private String expectedIndexPrefix() {
        return "product-83se";
    }
}