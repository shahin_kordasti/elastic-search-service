# Product Service
Search Service which uses elastic search to search and filter products.

# API documentation

Spring Rest Docs is used to generate API documentation as part of running the Component tests but to include it in the deployable you need to run:
`mvn clean install && mvn install -DskipITs`. The reason for the additional maven execution is because the doc is packaged in the deployable in the 
package phase and the component tests are run in the integration-test phase, which runs after the package phase, so the second run is for including
the generated documentation inside the deployable. Once this is done then run the application (see below) and the documentation can be found at 
`http://localhost:8282/product-service/docs/index.html`

# How to run
At project root run command: `docker-compose -f docker-compose.yaml -f docker-compose.service.yaml up`

If you want to run the application in IntelliJ then it is sufficient to run: `docker-compose up`
That will start Elastic Search and RabbitMQ locally and then after you can start `ProductServiceApplication` in the IDE. If done correctly the 
application can then be reached at for example: `http://localhost:8282/product-service/product/search`

PS

Remember to run the equivalent docker-compose down, when done, to cleanup containers and networks. 
E.g. `docker-compose -f docker-compose.yaml -f docker-compose.service.yaml down`

# Notes
Observe that this service is integrated with main product service through RabbitMQ. So when running locally, and not having a running and properly 
configured product service, then some special 'local' profiles needs to be set. If starting the service through the docker-compose 
files, this this is done automatically, but if you start the services in IntelliJ you have to add `local,stub` in the Active Profiles field (or
 `-Dspring.profiles.active=local,stub` if you are not using the Spring Boot plugin) in the run configuration of `ProductServiceApplication`.