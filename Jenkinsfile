import com.lenswaygroup.jenkins.lib.*

def confirmation = new Confirmation(this)
def deployment = new Deployment(this)
def shouldDeployUAT = false
def shouldDeployPROD = false

pipeline {
    agent any
    environment {
        PATH = "$PATH:/bin/:/usr/bin/:usr/local/bin/"
        REPO_NAME = "europa.lenslogistics.int"
        APP_NAME = "product-service"
        IMAGE_NAME = "$REPO_NAME/$APP_NAME"
        HOME = "."
        SECRET_LOCATION = "./CHANGEME_config${env.BUILD_ID}"
    }
    parameters {
        booleanParam(defaultValue: false, description: 'should deploy to uat', name: 'DEPLOY_UAT_ENV')
        booleanParam(defaultValue: false, description: 'should deploy to prod', name: 'DEPLOY_PROD_ENV')
        string(defaultValue: '', description: 'skips the build steps and deploy the specified docker image version',
                name: 'DEPLOY_ONLY')
    }
    tools {
        maven 'apache-maven-3'
    }
    stages {
        stage('Build') {
            when {
                expression { !params.DEPLOY_ONLY?.trim() }
            }
            steps {
                script {
                    sh 'mvn clean verify -Dspring.data.elasticsearch.cluster-nodes=10.7.7.3:9300 -Dspring.rabbitmq.host=10.7.7.2'
                    //below is to include the doc in the deployable
                    sh 'mvn verify -DskipITs'
                }
            }
        }
        stage('Create docker image') {
            when {
                expression { !params.DEPLOY_ONLY?.trim() }
            }
            steps {
                script {
                    sh 'docker login -u $EUROPA_USER -p $EUROPA_TOKEN  $REPO_NAME > /dev/null'
                    sh "docker build -t $IMAGE_NAME:latest \
                        -t $IMAGE_NAME:${env.BUILD_ID} \
                        -t $IMAGE_NAME:${env.GIT_COMMIT} \
                        -t $IMAGE_NAME:stage \
                          . "
                    sh "docker push $IMAGE_NAME:${env.BUILD_ID}"
                    sh "docker push $IMAGE_NAME:${env.GIT_COMMIT}"
                    sh "docker push $IMAGE_NAME:stage"
                    sh "docker push $IMAGE_NAME:latest"
                }
            }
        }
        stage('Deploy to stage kubernetes') {
            steps {
                script {
                    deployment.deployImageTo("product-service", "$APP_NAME", "$IMAGE_NAME", deployment.deployVersion())
                }
            }
        }
        stage('prepare ext kubeconfig') {
            when {
                expression { shouldDeployUAT || params.DEPLOY_UAT_ENV || shouldDeployPROD || params.DEPLOY_PROD_ENV }
            }
            steps {
                withCredentials([file(credentialsId: 'k8sProd', variable: 'KUBECONFIG')]) {
                    sh "rm -f $SECRET_LOCATION"
                    sh "cp $KUBECONFIG $SECRET_LOCATION"
                }
            }
        }
        stage('Promote to UAT') {
            when {
                expression { !params.DEPLOY_UAT_ENV }
            }
            steps {
                script {
                    shouldDeployUAT = confirmation.confirm('Deploy to UAT?', 10)
                }
            }
        }
        stage('deploy uat') {
            when {
                expression { shouldDeployUAT || params.DEPLOY_UAT_ENV }
            }
            steps {
//                script {
//                    deployment.deployImageTo("uat", "$APP_NAME", "$IMAGE_NAME", deployment.deployVersion(),
//                            "$SECRET_LOCATION")
//                }
                slackNotification("Deployed ${IMAGE_NAME}:${deployment.deployVersion()} to UAT", 'good')
            }
        }
        stage('Promote to PROD') {
            when {
                expression { !params.DEPLOY_PROD_ENV }
            }
            steps {
                script {
                    shouldDeployPROD = confirmation.confirm('Deploy to PROD?', 10)
                }
            }
        }
        stage('deploy prod') {
            when {
                expression { shouldDeployPROD || params.DEPLOY_PROD_ENV }
            }
            steps {
//                script {
//                    deployment.deployImageTo("optiportal", "$APP_NAME", "$IMAGE_NAME", deployment.deployVersion(),
//                            "$SECRET_LOCATION")
//                }
                slackNotification("Deployed ${IMAGE_NAME}:${deployment.deployVersion()} to PROD", 'good')
            }
        }
    }

    post {
        fixed {
            emailNotification("Build fixed for ${env.BUILD_NUMBER}",
                    "Please see the attached build log and ${env.BUILD_URL}")
            slackNotification("Build fixed for ${env.BUILD_NUMBER} URL - ${env.BUILD_URL}", 'good')
        }
        failure {
            emailNotification("Build Failed for ${env.BUILD_NUMBER}",
                    "Please see the attached build log and ${env.BUILD_URL}")
            slackNotification("Build Failed for ${env.BUILD_NUMBER} URL - ${env.BUILD_URL}", 'danger')
        }
        always {
            sh 'docker system prune -f'
            sh "rm -f $SECRET_LOCATION"
        }
    }
}

private void emailNotification(String subject, String bodyContent) {
    emailext attachLog: true,
            subject: subject,
            body: bodyContent,
            compressLog: true,
            recipientProviders: [culprits()],
            from: 'Jenkins <development@lenswaygroup.com>'
}

private void slackNotification(String message, String color){
    slackSend color: color, message: message
}
