FROM europa.lenslogistics.int/lwg_java8base:2019-05-14

WORKDIR /app

ADD target/product-service.jar /app/product-service.jar

ENTRYPOINT ["java","-Xmx750m", "-Xms500m", "-Dlogging.config=/app/log4j2.xml", "-Duser.timezone=Europe/Stockholm", "-jar", "/app/product-service.jar"]
